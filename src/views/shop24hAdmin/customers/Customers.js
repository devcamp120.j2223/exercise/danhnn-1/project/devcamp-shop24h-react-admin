import React, { useEffect, useState } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CTable,
  CTableBody,
  CTableCaption,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
} from '@coreui/react'
import { DocsExample } from 'src/components'

const Customers = () => {
  const [customers, setCustomes] = useState([])
  const fetchAPI = async (url) => {
    const response = await fetch(url)
    const data = response.json()
    return data
  }

  useEffect(() => {
    fetchAPI('http://localhost:8000/customers')
      .then((data) => {
        setCustomes(data.data)
        console.log(data)
      })
      .catch((error) => {
        console.log(error.message)
      })
  })

  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>React Table</strong> <small>Hoverable rows</small>
          </CCardHeader>
          <CCardBody>
            <p className="text-medium-emphasis small">
              Use <code>hover</code> property to enable a hover state on table rows within a{' '}
              <code>&lt;CTableBody&gt;</code>.
            </p>
            <DocsExample href="components/table#hoverable-rows">
              <CTable color="dark" striped hover>
                <CTableHead>
                  <CTableRow>
                    <CTableHeaderCell scope="col">STT</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Fullname</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Phone</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Email</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Address</CTableHeaderCell>
                    <CTableHeaderCell scope="col">City</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Country</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Orders</CTableHeaderCell>
                  </CTableRow>
                </CTableHead>
                <CTableBody>
                  {customers.map((customer, index) => {
                    return (
                      <CTableRow key={index}>
                        <CTableHeaderCell scope="row">{index + 1}</CTableHeaderCell>
                        <CTableDataCell>{customer.fullName}</CTableDataCell>
                        <CTableDataCell>{customer.phone}</CTableDataCell>
                        <CTableDataCell>@{customer.email}</CTableDataCell>
                        <CTableDataCell>@{customer.address}</CTableDataCell>
                        <CTableDataCell>@{customer.city}</CTableDataCell>
                        <CTableDataCell>@{customer.country}</CTableDataCell>
                        <CTableDataCell>@{customer.orders}</CTableDataCell>
                      </CTableRow>
                    )
                  })}
                </CTableBody>
              </CTable>
            </DocsExample>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default Customers
