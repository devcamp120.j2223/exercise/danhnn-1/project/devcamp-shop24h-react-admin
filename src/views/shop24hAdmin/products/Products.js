import React, { useState, useEffect } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CTable,
  CTableBody,
  CTableCaption,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
} from '@coreui/react'
import { DocsExample } from 'src/components'

const Products = () => {
  const [products, setProducts] = useState([])

  const fetchAPI = async (url) => {
    const response = await fetch(url)
    const data = response.json()
    return data
  }

  useEffect(() => {
    fetchAPI('http://localhost:8000/products')
      .then((data) => {
        setProducts(data.data)
        console.log(data)
      })
      .catch((error) => {
        console.error(error.message)
      })
  })

  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>React Table</strong> <small>Hoverable rows</small>
          </CCardHeader>
          <CCardBody>
            <p className="text-medium-emphasis small">
              Use <code>hover</code> property to enable a hover state on table rows within a{' '}
              <code>&lt;CTableBody&gt;</code>.
            </p>
            <DocsExample href="components/table#hoverable-rows">
              <CTable color="dark" striped hover>
                <CTableHead>
                  <CTableRow>
                    <CTableHeaderCell scope="col">STT</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Name</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Description</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Type</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Buy Price</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Promotion Price</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Amount</CTableHeaderCell>
                  </CTableRow>
                </CTableHead>
                <CTableBody>
                  {products.map((product, index) => {
                    return (
                      <CTableRow key={index}>
                        <CTableHeaderCell scope="row">{index + 1}</CTableHeaderCell>
                        <CTableDataCell>{product.name}</CTableDataCell>
                        <CTableDataCell>{product.description}</CTableDataCell>
                        <CTableDataCell>{product.type}</CTableDataCell>
                        <CTableDataCell>{product.buyPrice}</CTableDataCell>
                        <CTableDataCell>{product.promotionPrice}</CTableDataCell>
                        <CTableDataCell>{product.amount}</CTableDataCell>
                      </CTableRow>
                    )
                  })}
                </CTableBody>
              </CTable>
            </DocsExample>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default Products
